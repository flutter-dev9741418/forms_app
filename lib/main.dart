import 'package:flutter/material.dart';

import 'config/router/app.router.dart';
import 'config/themes/app.theme.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: AppTheme(
        selectColor: 2,
        mode: Brightness.light,
      ).theme(),
      routerConfig: appRouter,
    );
  }
}
