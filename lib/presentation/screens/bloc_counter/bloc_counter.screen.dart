import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forms_app/presentation/blocs/counter_bloc/counter_bloc.dart';

class BlocCounterScreen extends StatelessWidget {
  static const name = "BlocCounterScreen";

  const BlocCounterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterBloc(),
      child: const _BlocCounterView(),
    );
  }
}

class _BlocCounterView extends StatelessWidget {
  const _BlocCounterView();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: context.select((CounterBloc value) =>
            Text("Bloc Counter: ${value.state.transactionCount}")),
        actions: [
          IconButton(
            onPressed: () {
              context.read<CounterBloc>().add(CounterReseted());
            },
            icon: const Icon(Icons.refresh_rounded),
          ),
        ],
      ),
      body: Center(
        child: context.select((CounterBloc value) =>
            Text("Counter Value: ${value.state.counter}")),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: "3",
            child: const Text("+3"),
            onPressed: () {
              context.read<CounterBloc>().add(CounterIncreased(3));
            },
          ),
          const SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            heroTag: "2",
            child: const Text("+2"),
            onPressed: () {
              context.read<CounterBloc>().add(CounterIncreased(2));
            },
          ),
          const SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            heroTag: "1",
            child: const Text("+1"),
            onPressed: () {
              context.read<CounterBloc>().add(CounterIncreased(1));
            },
          ),
        ],
      ),
    );
  }
}
