import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forms_app/presentation/blocs/register_cubit/register_cubit.dart';
import 'package:forms_app/shared/shared.module.dart';

class RegisterScreen extends StatefulWidget {
  static const name = "RegisterScreen";

  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => RegisterCubit(),
      child: const _RegisterView(),
    );
  }
}

class _RegisterView extends StatelessWidget {
  const _RegisterView();

  @override
  Widget build(BuildContext context) {
    final registerCubit = context.watch<RegisterCubit>();
    final firstName = registerCubit.state.firstName;
    final lastName = registerCubit.state.lastName;
    final password = registerCubit.state.password;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Register Screen"),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: SingleChildScrollView(
            child: Form(
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  InputAppWidget(
                    type: TextInputType.text,
                    label: "Nombre",
                    iconLeft: Icons.people_alt_rounded,
                    placeholder: "Ej: José, Eliel, Frainzuanklin",
                    onChange: registerCubit.firstNameChanged,
                    errorMessage: firstName.errorMessage,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  InputAppWidget(
                    type: TextInputType.text,
                    label: "Apellido",
                    iconLeft: Icons.people_alt_rounded,
                    placeholder: "Ej: Hérnandez, Loreto, Vasquez",
                    onChange: registerCubit.lastNameChanged,
                    errorMessage: lastName.errorMessage,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  InputAppWidget(
                    type: TextInputType.visiblePassword,
                    label: "Contraseña",
                    iconRight: Icons.key_rounded,
                    placeholder: "**********",
                    onlyNumber: true,
                    typePass: true,
                    maxLength: 6,
                    onChange: registerCubit.passwordChanged,
                    errorMessage: password.errorMessage,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  FilledButton.tonalIcon(
                    onPressed: registerCubit.onSubmit,
                    icon: const Icon(Icons.save),
                    label: const Text("Crear usuario"),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
