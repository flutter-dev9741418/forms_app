import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:forms_app/infrastructure/inputs/inputs.module.dart';
import 'package:formz/formz.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterFormState> {
  RegisterCubit() : super(const RegisterFormState());

  void onSubmit() {
    emit(
      state.copyWith(
        formStatus: FormStatus.validating,
        firstName: NameInput.dirty(state.firstName.value),
        lastName: NameInput.dirty(state.lastName.value),
        password: PasswordInput.dirty(state.password.value),
        isValid: Formz.validate([
          state.firstName,
          state.lastName,
          state.password,
        ]),
      ),
    );
    debugPrint("onSubmit -< $state >-");
  }

  void firstNameChanged(String value) {
    final firstName = NameInput.dirty(value);

    emit(state.copyWith(
      firstName: firstName,
      isValid: Formz.validate([firstName]),
    ));
  }

  void lastNameChanged(String value) {
    final lastName = NameInput.dirty(value);

    emit(state.copyWith(
      lastName: lastName,
      isValid: Formz.validate([lastName]),
    ));
  }

  void passwordChanged(String value) {
    final password = PasswordInput.dirty(value);
    emit(state.copyWith(
      password: password,
      isValid: Formz.validate([password]),
    ));
  }
}
