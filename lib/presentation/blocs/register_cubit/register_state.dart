part of 'register_cubit.dart';

enum FormStatus { invalid, valid, validating, posting }

class RegisterFormState extends Equatable {
  final FormStatus formStatus;
  final bool isValid;
  final NameInput firstName;
  final NameInput lastName;
  final PasswordInput password;

  const RegisterFormState({
    this.formStatus = FormStatus.invalid,
    this.isValid = false,
    this.firstName = const NameInput.pure(),
    this.lastName = const NameInput.pure(),
    this.password = const PasswordInput.pure(),
  });

  copyWith({
    FormStatus? formStatus,
    bool? isValid,
    NameInput? firstName,
    NameInput? lastName,
    PasswordInput? password,
  }) {
    return RegisterFormState(
      formStatus: formStatus ?? this.formStatus,
      isValid: isValid ?? this.isValid,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [
        formStatus,
        isValid,
        firstName,
        lastName,
        password,
      ];
}
