import 'package:go_router/go_router.dart';
import 'package:forms_app/presentation/screens/screens.module.dart';

final appRouter = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
      name: HomeScreen.name,
      path: '/',
      builder: (context, state) {
        return const HomeScreen();
      },
    ),
    GoRoute(
      name: CubitCounterScreen.name,
      path: '/cubits',
      builder: (context, state) {
        return const CubitCounterScreen();
      },
    ),
    GoRoute(
      name: BlocCounterScreen.name,
      path: '/blocs',
      builder: (context, state) {
        return const BlocCounterScreen();
      },
    ),
    GoRoute(
      name: RegisterScreen.name,
      path: '/register',
      builder: (context, state) {
        return const RegisterScreen();
      },
    ),
  ],
);
