import 'package:formz/formz.dart';

enum PasswordInputError { empty, length }

class PasswordInput extends FormzInput<String, PasswordInputError> {
  const PasswordInput.pure() : super.pure('');

  const PasswordInput.dirty(String value) : super.dirty(value);

  String? get errorMessage {
    if (isValid || isPure) return null;

    if (displayError == PasswordInputError.empty) {
      return "El campo no puede estar vacío";
    }

    if (displayError == PasswordInputError.length) {
      return "El campo debe tener 6 carácteres";
    }

    return null;
  }

  @override
  PasswordInputError? validator(String value) {
    if (value.isEmpty || value.trim().isEmpty) {
      return PasswordInputError.empty;
    } else if (value.trim().length < 6) {
      return PasswordInputError.length;
    }

    return null;
  }
}
