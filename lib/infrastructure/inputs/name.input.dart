import 'package:formz/formz.dart';

enum NameInputError { empty, length }

class NameInput extends FormzInput<String, NameInputError> {
  const NameInput.pure() : super.pure('');

  const NameInput.dirty(String value) : super.dirty(value);

  String? get errorMessage {
    if (isValid || isPure) return null;

    if (displayError == NameInputError.empty) {
      return "El campo no puede estar vacío";
    }

    if (displayError == NameInputError.length) {
      return "El campo debe tener al menos 3 carácteres";
    }

    return null;
  }

  @override
  NameInputError? validator(String value) {
    if (value.isEmpty || value.trim().isEmpty) {
      return NameInputError.empty;
    } else if (value.trim().length < 3) {
      return NameInputError.length;
    }

    return null;
  }
}
